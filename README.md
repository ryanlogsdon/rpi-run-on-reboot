All we have to do to automatically run a program on reboot is:

terminal > crontab -e               (Note: don't use 'sudo crontab -e' !!)

then add this line:

@reboot python3 /path/to/my/script.py

or

@reboot /path/to/my/shell/script.sh

if you're using Python, you may need to play with how you invoke 'python3' (might need to add the full path), 
and in case you do need to add the full path, to find that path, just type 'which python' in the terminal